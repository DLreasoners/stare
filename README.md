# stare
****Description****

The project aims to develop a reasoner which is based on a new
optimization technique, called compressed-tableau. This reasoner allows
to reason on ontologies expressed in Description Logics (e.g. OWL
ontologies) containing link keys. A link key is a set of pairs of properties that identify equal entities of two classes.
For example **{(prenom ,firstname)**, **(nom, lastname)}** for **(Auteur, Author)** which states that if an instance **a** of the class **Auteur** and an instance **b** of the class **Author** share, resp., a value for the properties **prenom** and **firstname**, a value for **nom** and **lastname**, and a value for **ne** and **birthdate**, then they are the same entity. 

****Installation****
# Clone the repository.
# Compile

javac -cp "lib/\*" src/fr/paris13/limics/stare/\*.java -d bin

#Run
From the root folder
-------------------------
# Classpath is the bin folder that contains all the .class, in addition to all the needed libraries
java -classpath bin:lib/aopalliance-1.0.jar:lib/apache-log4j-1.2.15.jar:lib/commons-io-2.4.jar:lib/guava-18.0.jar:lib/guava-19.0.jar:lib/guice-3.0.jar:lib/guice-4.0.jar:lib/guice-assistedinject-3.0.jar:lib/guice-assistedinject-4.0.jar:lib/guice-multibindings-3.0.jar:lib/guice-multibindings-4.0.jar:lib/javax-inject.jar:lib/jsr305-3.0.0.jar:lib/log4j-1.2.17.jar:lib/owlapi-bin.jar:lib/owlapi-osgidistribution-4.1.0.jar:lib/sesame-model-2.7.16.jar:lib/sesame-rio-api-2.3.0.jar:lib/sesame-rio-api-2.7.16.jar:lib/sesame-rio-binary-2.7.16.jar:lib/sesame-rio-datatypes-2.7.16.jar:lib/sesame-rio-languages-2.7.16.jar:lib/sesame-rio-n3-2.7.16.jar:lib/sesame-rio-nquads-2.7.16.jar:lib/sesame-util-2.7.16.jar:lib/SizeOf.jar:lib/slf4j-api-1.7.9.jar:lib/slf4j-api-1.7.22.jar:lib/slf4j-log4j12-1.7.22.jar:lib/slf4j-simple-1.6.2.jar:lib/xz-1.5.jar fr.paris13.limics.stare.Main

# an easy way to concat all libraries 
java -cp bin:"$(printf %s: lib/*.jar)"  fr.paris13.limics.stare.Main



**Running the test data sets**
You can choose among 4 couples of ontologies and alignments:
(example1.owl, lkexample.rdf), (example2.owl,lkexample.rdf), (chainedLink.owl,LKSchainedLink.rdf), (consistency.owl, consistencyLK.rdf)
- Choose your ontology then press enter.
- Enter the name of alignment then press enter.
- If you want to display the matching function enter "y" then press enter.


****Work in progress****
- Testing with bigger data sets.


****Future work****
- Generating latex figures from the code.



****Next expected release****
- Friday 23/04/2021







